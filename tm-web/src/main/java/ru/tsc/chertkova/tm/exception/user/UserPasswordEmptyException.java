package ru.tsc.chertkova.tm.exception.user;

public class UserPasswordEmptyException extends AbstractUserException {

    public UserPasswordEmptyException() {
        super("Ошибка! Пароль пользователя не задан.");
    }

}
