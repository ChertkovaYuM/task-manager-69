package ru.tsc.chertkova.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
public enum RoleType {

    USUAL("Usual user"),

    ADMIN("Administrator"),

    TEST("Test");

    @Nullable
    private final String displayName;

    RoleType(@Nullable String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static RoleType toRole(@Nullable final String value) {
        if (value == null || value.isEmpty())
            return USUAL;
        for (RoleType role : values()) {
            if (role.name().equals(value))
                return role;
        }
        return USUAL;
    }

}
