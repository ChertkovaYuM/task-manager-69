package ru.tsc.chertkova.tm.exception.user;

public class UserNameAlreadyExistsException extends AbstractUserException {

    public UserNameAlreadyExistsException() {
        super("Ошибка! Пользователь уже существует.");
    }

    public UserNameAlreadyExistsException(final String userName) {
        super("Ошибка! Пользователь с логином `" + userName + "` уже существует.");
    }

}
