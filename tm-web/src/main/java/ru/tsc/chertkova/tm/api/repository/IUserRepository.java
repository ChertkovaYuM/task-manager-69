package ru.tsc.chertkova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.chertkova.tm.model.dto.UserDTO;

@Repository
public interface IUserRepository extends JpaRepository<UserDTO, String> {

    @Nullable
    UserDTO findFirstByLogin(@NotNull String login);

    @Nullable
    UserDTO findFirstById(@NotNull String id);

    @Nullable
    UserDTO findFirstByEmail(@NotNull String email);

    @Nullable
    @Modifying
    @Transactional
    UserDTO deleteByLogin(@NotNull String login);

    @Modifying
    @Transactional
    void deleteById(@Nullable String id);

}
