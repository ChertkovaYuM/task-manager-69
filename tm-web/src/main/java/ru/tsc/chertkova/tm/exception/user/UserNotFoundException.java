package ru.tsc.chertkova.tm.exception.user;

import org.jetbrains.annotations.NotNull;

public class UserNotFoundException extends AbstractUserException {

    public UserNotFoundException() {
        super("Ошибка! Пользователь не найден.");
    }

    public UserNotFoundException(@NotNull final String login) {
        super("Ошибка! Пользователь с логином '" + login + "' не найден в системе.");
    }

}
