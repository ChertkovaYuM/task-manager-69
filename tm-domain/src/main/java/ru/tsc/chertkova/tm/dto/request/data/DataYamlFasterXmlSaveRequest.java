package ru.tsc.chertkova.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.dto.request.user.AbstractUserRequest;

public final class DataYamlFasterXmlSaveRequest extends AbstractUserRequest {

    public DataYamlFasterXmlSaveRequest(@Nullable String token) {
        super(token);
    }

}
