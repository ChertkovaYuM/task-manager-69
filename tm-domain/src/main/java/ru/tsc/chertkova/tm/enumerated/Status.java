package ru.tsc.chertkova.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;

@Getter
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @Nullable
    final private String displayName;

    Status(@Nullable String displayName) {
        this.displayName = displayName;
    }

    @Nullable
    public static Status toStatus(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (Status status : values()) {
            if (status.name().equals(value)) return status;
        }
        return null;
    }

}
