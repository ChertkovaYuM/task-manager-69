package ru.tsc.chertkova.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import ru.tsc.chertkova.tm.api.endpoint.*;
import ru.tsc.chertkova.tm.api.service.IPropertyService;

@ComponentScan("ru.tsc.chertkova.tm")
public class ContextConfiguration {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Bean
    @NotNull
    public IDomainEndpoint adminEndpoint() {
        return IDomainEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());
    }

    @Bean
    @NotNull
    public IAuthEndpoint authEndpoint() {
        return IAuthEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());
    }

    @Bean
    @NotNull
    public IProjectEndpoint projectEndpoint() {
        return IProjectEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());
    }

    @Bean
    @NotNull
    public ITaskEndpoint taskEndpoint() {
        return ITaskEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());
    }

    @Bean
    @NotNull
    public IUserEndpoint userEndpoint() {
        return IUserEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());
    }

    @Bean
    @NotNull
    public ISystemEndpoint systemEndpoint() {
        return ISystemEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());
    }

    @Bean
    @NotNull
    public IDomainEndpoint domainEndpoint() {
        return IDomainEndpoint.newInstance(propertyService.getHost(), propertyService.getPort());
    }

}
